package com.project.api.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApacheConsumerAplication {

	public static void main(String[] args) {
		SpringApplication.run(ApacheConsumerAplication.class, args);
	}
}
